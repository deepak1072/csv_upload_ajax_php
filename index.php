<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 2:45 AM
 */   ?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
    <!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">
    <style type="text/css">
        .error{
            padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
        .navbar-brand {
            color: #26617d;
            margin-left: 23%;
            margin-bottom: 2%;

        }

        .navbar-brand:hover{
            color: #4c99ab;
        }

        .navbar-brand img{
            display: inline-block;
        }
        hr{
            border-color: #4e9aac;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
</head>
<body>






<div class="container">
    <h1 class="welcome text-center"> </h1>
    <div class="card card-container">

        <!--<h2 class='login_title text-center'>Login</h2>-->
        <a href="/" class="navbar-brand">


        </a>
        <hr>
        <p id="message" class="text-center alert  alert-danger error" hidden> </p>

        <form class="form-signin" id="uploadcsv" method="post"  >
            <span id="reauth-email" class="reauth-email"></span>
            <p class="input_title">Select CSV File To upload</p>
            <input type="file" id="csv" name="csv" class="login_box" placeholder="data.csv" required autofocus>



            <button class="btn btn-lg btn-primary" id="upload" type="button">upload</button>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->

<script type="text/javascript" src="/js/jquery.min.js"></script>

<script type="text/javascript">

    $(function () {

        /**
         * input validation
         */










                $("#csv").on("change",function(){
                    var name = $(this).val();

                    if(document.getElementById("csv").value.toLowerCase().lastIndexOf(".csv")==-1){

                        alert("only csv file allowed");
                        return false;
                    }
                });

                $("#upload").on("click",function(){


                    var formdata =  new FormData();
                    var csv = $("#csv").val();
                    if(csv == null || csv == undefined){
                        alert("please choose csv");
                        return false;
                    }
                    formdata.append("csv",document.getElementById("csv").files[0]);

                    $.ajax({
                        dataType: 'json',
                        processData: false, // important
                        contentType: false,
                        data : formdata,
                        type : "post",
                        url : "upload_csv.php",
                        success:function(response){
                            alert("fine");
                             alert(response);
                        }
                    });
                });






    });






    /**
     * function for error show
     */

    function Error(id,message,time) {

        $("#"+id).text(message).show();

        setTimeout(function () {
            $("#"+id).hide();
        },time);
    }





</script>

</body>
</html>
